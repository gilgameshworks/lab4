from pyspark import SparkContext, SparkConf
from pyspark.sql.session import SparkSession
from pyspark.ml import Pipeline
from pyspark.ml.clustering import KMeans
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.mllib.evaluation import MulticlassMetrics
from pyspark.ml.feature import Normalizer
from operator import add
import matplotlib.pyplot as plt
import os



os.environ['PYSPARK_PYTHON'] = "C:/NonOs/Apps/Anaconda/envs/MLE/python"

class Predictor():

    def __init__(self):
        spark = SparkSession.builder.appName("Lab4").getOrCreate()
        self.parquet_df = spark.read.parquet("food_clus_10.parquet")

    def data_scaler(self):
        assembler = VectorAssembler(inputCols=['energy_100g', 'proteins_100g', 'fat_100g',
       'carbohydrates_100g', 'sugars_100g', 'energy-kcal_100g',
       'saturated-fat_100g', 'salt_100g', 'sodium_100g', 'fiber_100g',
       'fruits-vegetables-nuts-estimate-from-ingredients_100g',
       'nutrition-score-fr_100g'], outputCol="features")
        df = assembler.transform(self.parquet_df)

        # Scale the features column
        scaler = StandardScaler(inputCol="features", outputCol="scaled_features", withStd=True, withMean=False)
        scaler_model = scaler.fit(df)
        self.df = scaler_model.transform(df)

    def pipeline_init(self):
        # модель кластеризации
        kmeans = KMeans(
            k=10,
            featuresCol='features',
            predictionCol='cluster10',
            seed=42
        )

        #модель регрессии
        logres = LogisticRegression(
            maxIter=30,
            family='multinomial',
            featuresCol='features',
            labelCol='cluster10',
            predictionCol='class'
        )

        #создание пайплайна
        self.pipeline = Pipeline(stages=[kmeans, logres])

    def pipeline_run(self):
        model = self.pipeline.fit(self.df)

        self.kmeans_info = model.stages[0].summary
        self.logres_info = model.stages[1].summary

    def print_learn_results(self):
        print(f'Cluster sizes: {self.kmeans_info.clusterSizes}')
        print(f'Accuracy: {self.logres_info.accuracy}')
        print(f'Precision: {self.logres_info.weightedPrecision}')
        print(f'Recall: {self.logres_info.weightedRecall}')
        print(f'TPR: {self.logres_info.weightedTruePositiveRate}')
        print(f'TNR: {self.logres_info.weightedFalsePositiveRate}')
        print(f'FPR: {self.logres_info.weightedFalsePositiveRate}')
        print(f'FNR: {self.logres_info.weightedFalsePositiveRate}')
        plt.title('График регрессии')
        plt.plot(self.logres_info.objectiveHistory, linestyle = 'dotted')
        plt.show()

    def cofusion_matrix(self):
        df_prediction = self.model.transform(self.df)
        new_df = df_prediction.selectExpr("cluster10", "class")
        metrics = MulticlassMetrics(new_df.rdd)

        # Вывод матрицы ошибок
        confusionMatrix = metrics.confusionMatrix()
        print(confusionMatrix)

    def confidence(self):
        df_prediction = self.model.transform(self.df)
        df_normed = df_prediction.rdd.map(lambda x: (x['class'], x['probability'])) \
    .reduceByKey(add).toDF(['class', 'probability'])

        norm_func = Normalizer(inputCol='probability', outputCol='normed', p=1)
        norm_func = norm_func.transform(df_normed).rdd.map(lambda x: (x['class'], x['norm_func'].toArray().max()))

        info = []
        for label, confidence in norm_func.collect():
            info.append([int(label), confidence]) 

        info = sorted(info, key=lambda x: x[0])
        for l, c in info:
            print(f'Class: {l} confidence: {c}')

if __name__=='main':
    predictor = Predictor()
    predictor.data_scaler()
    predictor.pipeline_init()
    predictor.pipeline_run()
    predictor.print_learn_results()
    predictor.confidence()

    